# Jenkins Pipeline
We are levereging Jenkins to make this process a one click deployment. The Pipeline connects to our bitbucket repository and pulls out the code. We are using aws cli to perform all the steps.
The source code (index.html) is copied to S3 which will be eventually served to our server(ec2) in our cfn template.
Then the cloudformation stack is created which will create all the infra for us and deploy app to our created server.
There is another stage to describe the created instance. We wait for 10 minutes for the instance to get created and initialised before describing it.

USAGE: Give the desired 'stack name' you want to create in parameters and build. Alternatively, this Jenkins' pipeline job watches this repository and is automatically triggered if a new commit is pushed in any of the source code.


# CLOUDFORMATION TEMPLATE
The cloudformation template will create a stack with all the required resources for our application on AWS. Here we are creating a security group and an ec2 instance. The Security group open access to port 80 for web traffic.
EC2 instance is being spinned up from a raw amazon linux ami. We configure it as a web server by passing pre initializing user data. Also, we are deploying the source code(index.html in our case) to this ec2 instance from S3 which was placed to S3 earlier by our groovy pipeline.

# INDEX.HTML
A sample index.html file that renders a webpage "Automation for the people"


# Jenkins Server URL
http://3.216.71.137:8080

# Created Web Server
http://54.91.79.10/



